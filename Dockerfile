FROM nginx:latest

ENV workdir /app/blogs-pront/

COPY . ${workdir}
WORKDIR ${workdir}

CMD ["nginx","-g","daemon off;"]